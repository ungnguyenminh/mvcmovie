﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        //
        //GET : /HelloWorld/
        public ActionResult Index()
        {
            //return "This is my <b>default</b> action..";
            return View();
        }
        //
        //GET: /HelloWorld/Welcome/
        // public string Welcome()
        //{
        //    return "This is the Welcome action method...";
        //}

        ////GET: /HelloWorld/Welcome?name=Scot&numtimes=4
        //public string Welcome(string name, int numTimes = 1)
        //{
        //    return HttpUtility.HtmlEncode("Hello " + name + ", NumTime is: " + numTimes);

        //}

        //GET: /HelloWorld/Welcome?name=Scot&numtimes=4
        //public string Welcome(string name, int id = 1)
        //{
        //    return HttpUtility.HtmlEncode("Hello " + name + ", ID is: " + id);

        //}
        public ActionResult Welcome(string name, int numTimes = 1)
        {
            ViewBag.Message = "Hello " + name;
            ViewBag.NumTimes = numTimes;

            return View();

        }
        public ActionResult Hello()
        {
            return View();
        }
    }
}