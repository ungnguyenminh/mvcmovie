﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class FilmController : Controller
    {
        private MovieDBContext db = new MovieDBContext();

        // GET: Film
        public ActionResult Index(string filmGenre, string searchString)
        {
            // Get genre list 
            var GenreList = new List<String>();
            var GenreQry = from g in db.Movies
                           orderby g.Genre
                           select g.Genre;

            GenreList.AddRange(GenreQry.Distinct());
            ViewBag.filmGenre = new SelectList(GenreList);

            var film = from m in db.Movies
                       select m;
            if (!string.IsNullOrEmpty(filmGenre))
            {
                film = film.Where(g => g.Genre.Contains(filmGenre));

            }

            if (!string.IsNullOrEmpty(searchString))
            {
                film = film.Where(m => m.Title.Contains(searchString));
            }
            return View(film);
        }
        // GET: Film/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: Film/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID, Title, ReleaseDate, Genre, Price, Rating")] Movie film)
        {
            if (ModelState.IsValid)
            {
                db.Movies.Add(film);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(film);
        }
        // GET: Film/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.Error = "Id is not avaiable!";
                return View();
            }
            Movie film = db.Movies.Find(id);
            if (film == null)
            {
                ViewBag.Error = "Film is not avaiable!";
                return View();
            }
            return View(film);
        }
        // POST: Film/Edit/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Movie film)
        {
            if (ModelState.IsValid)
            {
                db.Entry(film).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.Error = "Edit error";
            return View(film);
        }

        // GET: Film/Detail/1
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                ViewBag.Error = "Id is not avaiavle!";
                return View();
            }
            Movie film = db.Movies.Find(id);
            if (film == null)
            {
                ViewBag.Error = "Film is not avaiavle!";
                return View();
            }
            return View(film);
        }

        // GET: Film/Delete/1
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.Error = "Id is not avaiable!";
                return View();
            }
            Movie film = db.Movies.Find(id);
            if (film == null)
            {
                ViewBag.Error = "Film is not avaiable!";
                return View();
            }
            return View(film);

        }

        // POST: Film/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(FormCollection fcNotUsed, int id = 0)
        {
            Movie film = db.Movies.Find(id);
            if (film == null)
            {
                ViewBag.Error = "Film is not delete!";
                return View();
            }
            db.Movies.Remove(film);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}