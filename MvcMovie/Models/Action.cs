﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class Action
    {
        public int ID { get; set; }

        [Display(Name = "Full Name")]
        [Required, StringLength(60, MinimumLength = 3)]
        public string FullName { get; set; }

        [Display(Name = "BirthDay"), Required, DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDay { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public int MovieId { get; set; }


    }

    //public class Action : DbContext
    //{
    //    public DbSet<Action> Actions { get; set; }
    //}
}